import pandas as pd
import datetime
import sys

PATH = '../data/'

COLLGATE_URL = ""
DEST_PATH = ""


def hour_range(start, end):
    """
    Range of hours between two datetimes
    :param start:
    :param end:
    """
    span = end - start
    for i in range(0, int((span.seconds / 3600)) + 1):
        yield start + datetime.timedelta(hours=i)


def trim(df, max_time):
    """
    Only keeps segments values between 0 and max_time
    :param df:
    :param max_time:
    :return:
    """

    # reset index to avoid duplicates
    df = df.reset_index().drop(columns=['index'])

    # trim to 0 for segments starting before selection
    idx = df[(df.start < 0) & (df.stop > 0)].index
    if len(idx) > 0:
        df.loc[idx[0], 'start'] = 0

    #  trim to max_time for segments ending after selection
    idx = df[(df.start < max_time) & (df.stop > max_time)].index
    if len(idx) > 0:
        df.loc[idx[0], 'stop'] = max_time

    return df[(df.start >= 0) & (df.stop <= max_time)]


def add_missing(df, file, media, channel):
    if df is None:
        df = pd.DataFrame(columns=['source_path', 'dest_path'])
    s = file.split('-')
    df = df.append({
        'source_path': "%s%s/%s/%sT%s/%s?download&format=ts_audio" % (COLLGATE_URL, media, channel, s[2], s[3], s[4]),
        'dest_path': "%s%s/%s/%s/%s" % (DEST_PATH, media, channel, s[2][:4], file)
    }, ignore_index=True)
    return df


def load_timerange(medium, channel, start_time, end_time):
    """
    Loads a dataframe of segments between start_time and end_time + computes duration of each segment
    :rtype: pd.DataFrame, pd.DataFrame
    :param medium: tv or radio
    :type medium: str
    :param channel: name of the channel
    :type channel: str
    :param start_time: start of time window
    :type start_time: datetime.datetime
    :param end_time: end of time window
    :type end_time: datetime.datetime
    :return:
    """
    # dirty hack to avoid loading next hour if not necessary -- FIXME
    end_time = end_time - datetime.timedelta(minutes=1)

    filename_header = medium + '-' + channel + '-'
    files = [
        filename_header + d.date().strftime('%Y%m%d') + '-' + d.time().strftime('%H') + '0000-3600.csv'
        for d in hour_range(start_time, end_time)
    ]

    remove_before = start_time.minute * 60
    max_time = (end_time - start_time).seconds

    df = pd.DataFrame()
    missing = None
    for i, f in enumerate(files):
        try:
            tmp = pd.read_csv(PATH + f, sep='\t')
            tmp.start = tmp.start + (3600 * i) - remove_before
            tmp.stop = tmp.stop + (3600 * i) - remove_before
        except FileNotFoundError:
            print('No data for %s' % f, file=sys.stderr)
            tmp = pd.DataFrame([['unavailable', (3600 * i) - remove_before, (3600 * (i + 1)) - remove_before]],
                               columns=['labels', 'start', 'stop'])
            missing = add_missing(missing, f, medium, channel)

        df = df.append(tmp)

    df = trim(df, max_time)

    df['duration'] = df.stop - df.start

    return df.reset_index().drop(columns=['index']), missing


def save_to_csv(start_time, end_time, channel, medium='tv', name=None, only_sums=False, get_missing=True):
    """

    :param start_time:
    :param end_time:
    :param channel:
    :param medium: tv or radio
    :param name:
    :param only_sums: Only save the sums csv
    :param get_missing: Generate a csv for missing files
    """
    s = datetime.datetime.fromisoformat(start_time)
    e = datetime.datetime.fromisoformat(end_time)
    df, missing = load_timerange(medium, channel, s, e)
    if name is None:
        name = channel + '-' + start_time + '-' + end_time

    df.groupby('labels').duration.sum().to_csv('sums-' + name + '.csv', sep='\t')
    if not only_sums:
        df.to_csv(name + '.csv', sep='\t')

    if get_missing and missing is not None:
        missing.to_csv(name + '-missing.csv', sep='\t')


def save_to_pkl(start_time, end_time, channel, medium='tv', name=None, only_sums=False, get_missing=True):
    """

    :param start_time:
    :param end_time:
    :param channel:
    :param medium: tv or radio
    :param name:
    :param only_sums: Only save the sums pickled DF
    :param get_missing: Generate a csv for missing files
    """
    s = datetime.datetime.fromisoformat(start_time)
    e = datetime.datetime.fromisoformat(end_time)
    df, missing = load_timerange(medium, channel, s, e)
    if name is None:
        name = channel + '-' + start_time + '-' + end_time

    df.groupby('labels').duration.sum().to_pickle('sums-' + name + '.pkl')
    if not only_sums:
        df.to_csv(name + '.pkl')

    if get_missing and missing is not None:
        missing.to_csv(name + '-missing', sep='\t')


if __name__ == '__main__':
    s = datetime.datetime(2020, 2, 4, 14)
    e = datetime.datetime(2020, 2, 4, 15)

    save_to_csv(s.isoformat(), e.isoformat(), 'FR2', name='fr2-14-15')

    exit(0)
